## Parashift Comment Index

This module allows comments to be indexed alongside nodes.  When you search for a comment, a document is returned.

### Building and Compiling

You will need the [alfresco amp plugin](https://bitbucket.org/parashift/alfresco-amp-plugin)

You will also need gradle

* Edit your global `~/.gradle/gradle.properties` and add in the `alfresco.repo.private.username` and `alfresco.repo.private.password` properties for the private maven alfresco repo

* Run `gradle amp` in the directory:

``` 
gradle amp
```

### Installing

Install the AMP located under build/amp
